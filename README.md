#XPacks

Pacote de funções para auxiliar o desenvolvimento da Rtools Sistemas

* [Análise](#analise)
* [Block](#block)
* [DataHoje](#datahoje)
* [Debugs](#debugs)
* [FileIS](#filesis)
* [Logs](#logs)
* [Mac](#mac)
* [Mask](#mask)
* [Moeda](#moeda)
* [Path](#path)
* [Ping](#ping)
* [Preloader](#preloader)
* [PreloaderDialog](#preloaderdialog)
* [Properties](#properties)
* [Property](#property)
* [Sessions](#sessions)
* [Types](#types)
* [ValidaDocumentos](#validadocumentos)
* [Validation](#validation)