package xpacks;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

public class FileIS {

    public static void saveImage(String imageUrl, String destinationFile, Boolean close) {
        try {
            saveImage(new URL(imageUrl).openStream(), destinationFile, close);
        } catch (Exception e) {

        }
    }

    public static void saveImage(InputStream is, String destinationFile, Boolean close) {
        try (OutputStream os = new FileOutputStream(destinationFile)) {
            byte[] b = new byte[2048];
            int length;

            while ((length = is.read(b)) != -1) {
                os.write(b, 0, length);
            }
            if (close) {
                is.close();
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}
