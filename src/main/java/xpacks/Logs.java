package xpacks;

import java.awt.Desktop;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import static xpacks.Property.getAppData;

public class Logs {

    private String current_date = null;
    private File file = null;
    private String path;

    protected String create_file(String filename) {
        try {
            String path = Path.getAppData() + File.separator + "logs" + File.separator;
            File f = new File(path);
            if (!f.exists()) {
                f.mkdirs();
            }
//            String canonicalPath = "";
//            try {
//                canonicalPath = new File(".").getCanonicalPath();
//            } catch (IOException ex) {
//            }
//            // String path = canonicalPath + File.separator + "var" + File.separator + "log";
//            File f = new File(path);
//            if (!f.exists()) {
//                path = "C:" + File.separator + developer + File.separator + appname + File.separator + "var" + File.separator + "log";
//                f = new File(path);
//                if (!f.exists()) {
//                    f.mkdirs();
//                }
//            }
            String day = (DataHoje.data()).substring(0, 2);
            String month = (DataHoje.data()).substring(3, 5);
            String year = (DataHoje.data()).substring(6, 10);
            current_date = year + "_" + month + "_" + day;
            String c1 = path + File.separator;
            File fileData = new File(c1);
            if (!fileData.exists()) {
                fileData.mkdir();
            }
            String c2 = path + File.separator + current_date + "_" + filename + ".log";
            file = new File(c2);
            if (!file.exists()) {
                FileOutputStream fileData2 = new FileOutputStream(c2);
                fileData2.close();
            }
        } catch (Exception e) {
            System.err.println("XPacks: " + e.getStackTrace());
        }
        return null;
    }

    public void store(String location, String content) {
        try {
            location = location.toLowerCase();
            location = location.replace(" ", "_");
            location = location.replace(">", "_");
            location = location.replace(".", "_");
            location = location.replace("-", "_");
            location = location.replace("|", "_");
            location = location.replace(")", "_");
            location = location.replace("(", "_");
            create_file(location);
            if (file != null && file.exists()) {
                content = DataHoje.horaMinuto() + " >> " + content;
                FileWriter writer = new FileWriter(file, true);
                try (BufferedWriter buffWriter = new BufferedWriter(writer)) {
                    buffWriter.write(content);
                    buffWriter.newLine();
                    buffWriter.flush();
                }
            }
        } catch (IOException e) {
            System.err.println("XPacks: " + e.getStackTrace());
        }
    }

    public static void openLocalFolder() {
        String path = Path.getAppData() + File.separator + "logs" + File.separator;
        File local = new File(path);
        if (!local.exists()) {
            local.mkdirs();
        }
        try {
            Desktop.getDesktop().open(local);
        } catch (IOException ioe) {
            System.err.println("XPacks: " + ioe.getStackTrace());
        }
    }

    protected File getFile() {
        return file;
    }

    protected void setFile(File file) {
        this.file = file;
    }
}
