package xpacks;

import java.io.File;

public class Path {

    public static String getUserPath() {
        String path = "";
        String os_name = Property.getOSName().toLowerCase();
        String app_data = System.getenv("APPDATA");
        if (os_name.contains("win") || os_name.toLowerCase().contains("windows")) {
            return app_data;
        } else if (os_name.contains("sunos")) {
            return app_data;
        } else if (os_name.contains("linux")) {
            if (app_data == null || app_data.isEmpty()) {
                app_data = "logs";
            }
            return app_data;
        } else if (os_name.contains("nix") || os_name.contains("nux") || os_name.contains("aix")) {
            return app_data;
        } else if (os_name.contains("mac")) {
            return app_data;
        } else if (os_name.contains("centos")) {
            return app_data;
        }
        try {
        } catch (Exception e) {
            System.err.println("XPacks: " + e.getStackTrace());
            return null;
        }
        return path;
    }

    public static String getAppData() {
        String developer = new Property().getAppDeveloper();
        if (developer == null || developer.isEmpty()) {
            developer = "brunovschettini";
        }
        String appname = new Property().getAppName();
        if (appname == null || appname.isEmpty()) {
            appname = "xpacks";
        }
        developer = developer.toLowerCase();
        appname = appname.toLowerCase();
        return getUserPath() + File.separator + developer + File.separator + appname + File.separator;
    }

    public static String getRealPath() {
        String path = "";
        try {
            path = new File(".").getCanonicalPath();
        } catch (Exception ex) {
        }
        path = path.replace("\\", "\\\\");
        return path;
    }
}
