/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xpacks;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author rtools2
 */
public class Ping {

    public static void execute() {
        Logs logs = new Logs();
        try {
            // Endereço IP a ser verificado
            String ip = Inet4Address.getLocalHost().getHostAddress();
            if (ip.equals("127.0.0.1")) {
                JOptionPane.showMessageDialog(null, "Verifique sua conexão de rede!");
                logs.store("Erro na rede", "Rede local desabilitada!");
                System.exit(0);
            }
        } catch (UnknownHostException ex) {
            JOptionPane.showMessageDialog(null, "Servidor da remoto encontra-se inátivo! Possível causa: A rede central esta com problemas. DNS?. Acesso a internet está ok?");
            logs.store("Erro na rede", "Rede não esta ativa. Verifique as conexão de rede! " + ex.getMessage());
            Logger.getLogger(Ping.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }
    }

    public static void executeWaiting() {
        Logs logs = new Logs();
        for (int i = 0; i < 20; i++) {
            try {
                Thread.sleep(1 * 60 * 100);
            } catch (Exception et) {

            }
            try {
                // Endereço IP a ser verificado
                String ip = Inet4Address.getLocalHost().getHostAddress();
                if (ip.equals("127.0.0.1")) {
                    if (i == 20) {
                        logs.store("Erro na rede", "Rede local desabilitada!");
                        System.exit(0);
                    }
                } else {
                    break;
                }
                try {
                    Thread.sleep(1 * 60 * 1000);
                } catch (Exception et) {

                }
            } catch (UnknownHostException ex) {
                JOptionPane.showMessageDialog(null, "Servidor da remoto encontra-se inátivo! Possível causa: A rede central esta com problemas. DNS?. Acesso a internet está ok?");
                logs.store("Erro na rede", "Rede não esta ativa. Verifique as conexão de rede! " + ex.getMessage());
                Logger.getLogger(Ping.class.getName()).log(Level.SEVERE, null, ex);
                System.exit(0);
            }
        }
    }
}
