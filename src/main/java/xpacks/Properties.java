package xpacks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Properties {

    public String filename;
    public java.util.Properties properties = null;

    public Properties(String filename) {
        this.filename = "";
        this.filename = filename;
        this.properties = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filename);
            properties = new java.util.Properties();
            try {
                properties.load(fis);
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(fis != null) {
                fis.close();                    
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Properties.class.getName()).log(Level.SEVERE, "XPacks", ex);
        } catch (IOException ex) {
            Logger.getLogger(Properties.class.getName()).log(Level.SEVERE, "XPacks", ex);
        }
    }

    public String getString(String key) {
        try {
            return (String) (properties.getProperty(key));
        } catch (Exception e) {
            return null;
        }
    }

    public Boolean getBoolean(String key) {
        try {
            return Boolean.parseBoolean(properties.getProperty(key));
        } catch (Exception e) {
            return null;
        }
    }

    public Integer getInt(String key) {
        return getInteger(key);
    }

    public Integer getInteger(String key) {
        try {
            return Integer.parseInt(properties.getProperty(key));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public Double getDouble(String key) {
        try {
            return Double.parseDouble(properties.getProperty(key));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public Float getFloat(String key) {
        try {
            return Float.parseFloat(properties.getProperty(key));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public BigDecimal getBigDecimal(String key) {
        try {
            return new BigDecimal(properties.getProperty(key));
        } catch (NumberFormatException e) {
            return null;
        }
    }

}
